#!/bin/bash

# Start Gunicorn processes
echo Starting Gunicorn.
exec gunicorn  app:app \
    --name auth-api \
    --bind 0.0.0.0:5859 \
    --log-level=info \
     --worker-class aiohttp.GunicornWebWorker
