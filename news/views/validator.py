from marshmallow import fields, Schema, validates, ValidationError


class NewsSchema(Schema):
    header = fields.String(
        required=True,
        error_messages={'required': 'Header is required'})
    category = fields.Integer(
        required=True,
        error_messages={'required': 'Category is required'}
    )
    content = fields.String(
        required=True,
        error_messages={'required': 'Content is required'}
    )

    @validates('header')
    def validate_header(self, value):
        if len(value) < 5:
            print(1)
            raise ValidationError('Header must be greater than 5')

    @validates('content')
    def validate_content(self, value):
        if len(value) < 15:
            print(1)
            raise ValidationError('Content must be greater than 10')
