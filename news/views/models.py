import asyncio
from app import app
from datetime import datetime


class News:
    def __init__(self, data):
        self.data = data

    async def get(self):
        cursor = app.db.news.find({}, {'_id': False})
        result = []
        async for doc in app.db.news.find({}, {'_id': False}):
            result.append(doc)
        res_len = await app.db['news'].count_documents({})
        print(result)
        response = await cursor.to_list(res_len)
        return response

    async def add(self):
        try:
            date = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S")
            header = self.data['header']
            content = self.data['content']
            category = self.data['category']
            last_id_obj = app.db['news'].find().sort([("_id", -1)]).limit(1)
            last_id = ([dict(r) async for r in last_id_obj])
            if len(last_id) is not 0:
                last_id = int(last_id[0]['id'])
                next_id = last_id+1
            else:
                last_id = 0
                next_id = 1
            result = await app.db['news'].insert_one({
                'id': int(next_id),
                'header': header,
                'content': content,
                'category': category,
                'date': date
            })
            if result.inserted_id is not None:
                return True
            else:
                return False
        except:
            return False

    async def edit(self):
        try:
            result = await app.db['news'].update_one(
                {"id": int(self.data['id'])},
                {
                    "$set":
                    {
                        'category': self.data['category'],
                        'header': self.data['header'],
                        'content': self.data['content']
                    }
                }
            )
            return True
        except:
            return False

    async def remove(self):
        try:
            result = await app.db['news'].delete_many({'id': int(self.data['id'])})
            return True
        except:
            return False
