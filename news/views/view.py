import json
from marshmallow import ValidationError
from aiohttp import web, request
from route_table import routes
from views.models import News
from views import validator


@routes.view('/news')
class UserView(web.View):

    async def get(self):
        data = {}
        news = News(data)
        return web.json_response(await news.get())

    async def post(self):
        data = await self.request.text()
        # print(data)
        try:
            schema = validator.NewsSchema()
            try:
                data = json.loads(data)
                print(data)
            except:
                return web.json_response({'status': "false",
                                          'errors': {
                                              'jsonParseError': 'params is required'
                                          }})
            result = schema.load(data)
            if len(result.errors) is 0:
                news = News(result.data)
                if await news.add() is True:
                    return web.json_response({'status': "true"})
                else:
                    return web.json_response({'status': "false"})
            else:
                return web.json_response({'status': "false", 'errors': result.errors})
        except ValidationError as err:
            print(err.messages)
            return web.json_response({'status': "false", 'errors': err.messages})

    async def put(self):
        data = await self.request.text()
        try:
            schema = validator.NewsSchema()
            try:
                data = json.loads(data)
            except:
                return web.json_response({'status': "false",
                                          'errors': {
                                              'jsonParseError': 'params is required'
                                          }})
            result = schema.load(data)
            if len(result.errors) is 0:
                news = News(data)
                if await news.edit() is True:
                    return web.json_response({'status': "true"})
                else:
                    return web.json_response({'status': "false"})
            else:
                return web.json_response({'status': "false", 'errors': result.errors})
        except ValidationError as err:
            print(err.messages)
            return web.json_response({'status': "false", 'errors': err.messages})

    async def delete(self):
        data = await self.request.text()
        news = News(data)
        if await news.remove() is True:
            return web.json_response({'status': "true"})
        else:
            return web.json_response({'status': "false"})

