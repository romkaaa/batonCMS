import json
from marshmallow import ValidationError
from aiohttp import web, request
from route_table import routes
from views.models import Categories
from views import validator


@routes.view('/categories')
class UserView(web.View):

    async def get(self):
        data = {}
        categories = Categories(data)
        return web.json_response(await categories.get())

    async def post(self):
        data = await self.request.text()
        try:
            schema = validator.AddCategoriesSchema()
            try:
                data = json.loads(data)
                print(data)
            except:
                return web.json_response({'status': "false",
                                          'errors': {
                                              'jsonParseError': 'params is required'
                                          }})
            result = schema.load(data)
            if len(result.errors) is 0:
                categories = Categories(result.data)
                add_categories_status = await categories.add()
                if add_categories_status is True:
                    return web.json_response({'status': "true"})
                else:
                    return web.json_response(json.loads(add_categories_status))
            else:
                return web.json_response({'status': "false", 'errors': result.errors})
        except ValidationError as err:
            print(err.messages)
            return web.json_response({'status': "false", 'errors': err.messages})

    async def put(self):
        data = await self.request.text()
        try:
            schema = validator.EditCategoriesSchema()
            try:
                data = json.loads(data)
            except:
                return web.json_response({'status': "false",
                                          'errors': {
                                              'jsonParseError': 'params is required'
                                          }})
            result = schema.load(data)
            if len(result.errors) is 0:
                categories = Categories(data)
                if await categories.edit() is True:
                    return web.json_response({'status': "true"})
                else:
                    return web.json_response({'status': "false"})
            else:
                return web.json_response({'status': "false", 'errors': result.errors})
        except ValidationError as err:
            print(err.messages)
            return web.json_response({'status': "false", 'errors': err.messages})

    async def delete(self):
        data = await self.request.text()
        print(data)
        categories = Categories(data)
        if await categories.remove() is True:
            return web.json_response({'status': "true"})
        else:
            return web.json_response({'status': "false"})

