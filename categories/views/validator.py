from marshmallow import fields, Schema, validates, ValidationError


class EditCategoriesSchema(Schema):
    name = fields.String(
        required=True,
        error_messages={'required': 'Name is required'})
    new_name = fields.Integer(
        required=True,
        error_messages={'required': 'New-name is required'}
    )

    @validates('name')
    def validate_name(self, value):
        if len(value) < 1:
            print(1)
            raise ValidationError('Name must be greater than 1')

    @validates('new_name')
    def validate_new_name(self, value):
        if len(value) < 1:
            print(1)
            raise ValidationError('New_name must be greater than 1')


class AddCategoriesSchema(Schema):
    name = fields.String(
        required=True,
        error_messages={'required': 'Name is required'})

    @validates('name')
    def validate_name(self, value):
        if len(value) < 1:
            print(1)
            raise ValidationError('Name must be greater than 1')
