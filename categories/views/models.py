import json
from app import app
from datetime import datetime


class Categories:
    def __init__(self, data):
        self.data = data

    async def get(self):
        cursor = app.db.categories.find({}, {'_id': False})
        # result = []
        # async for doc in app.db.cate.find({}, {'_id': False}):
        #     result.append(doc)
        res_len = await app.db['categories'].count_documents({})
        response = await cursor.to_list(res_len)
        return response

    async def add(self):
        try:
            date = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S")
            name = self.data['name']
            find_result_len = await app.db['categories'].count_documents({'name': name})
            if find_result_len is not 0:
                return json.dumps({
                    'status': "false",
                    'error': "category is exists"
                })
            last_id_obj = app.db['categories'].find().sort([("_id", -1)]).limit(1)
            last_id = ([dict(r) async for r in last_id_obj])
            if len(last_id) is not 0:
                last_id = int(last_id[0]['id'])
                next_id = last_id+1
            else:
                last_id = 0
                next_id = 1
            result = await app.db['categories'].insert_one({
                'id': int(next_id),
                'name': name,
                'date': date
            })
            if result.inserted_id is not None:
                return True
            else:
                return False
        except:
            return False

    async def edit(self):
        try:
            result = await app.db['categories'].update_one(
                {"name": int(self.data['name'])},
                {
                    "$set":
                    {
                        'name': self.data['new_name']
                    }
                }
            )
            return True
        except:
            return False

    async def remove(self):
        self.data = json.loads(self.data)
        name = self.data['name']
        try:
            await app.db['categories'].delete_one({'name': name})
            await app.db['news'].delete_many({'category': name})
            return True
        except:
            return False
