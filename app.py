import logging
import sys
from urllib.parse import urljoin

import asyncio
import aiohttp
from aiohttp import web

logger = logging.getLogger("runproxy")


async def proxy(request):
    port = 80
    route = request.match_info['path']
    if route == '/categories':
        port = 5757
    if route == 'users':
        port = 5858
    if route == 'news':
        port = 5859

    print(port)
    TARGET_SERVER_BASE_URL = 'http://127.0.0.1:%s' % str(port)
    target_url = urljoin(TARGET_SERVER_BASE_URL, request.match_info['path'])
    data = await request.read()
    get_data = request.rel_url.query

    async with aiohttp.ClientSession() as session:

        async with session.request(request.method, target_url, headers=request.headers, params=get_data, data=data) as resp:
            res = resp
            raw = await res.read()

    return web.Response(body=raw, status=res.status, headers=res.headers)


logging.root.setLevel(logging.INFO)
logging.root.addHandler(logging.StreamHandler(sys.stdout))

app = web.Application()
app.router.add_route('*', '/{path:.*?}', proxy)

loop = asyncio.get_event_loop()
f = loop.create_server(app.make_handler(), '0.0.0.0', 8090)
srv = loop.run_until_complete(f)
print('serving on', srv.sockets[0].getsockname())
try:
    loop.run_forever()
except KeyboardInterrupt:
    pass