## API

Microservice-architectured api
Services: news, auth, categories, products, orders
---

#### Requirements


*`docker`
---
#### Configuration variables:



*`MONGO_HOST` - this is the name of the container where mongo is running
*`AUTH_PORT`: this is the name of port where auth service is running
---
#### Run
At first you should build services from their Dockerfiles
At the / of project: 
##### auth:
`docker build -t camp-auth-api ./auth`
##### news:
`docker build -t camp-news_api ./news`
##### categories:
`docker build -t camp-categories-api ./categories`

After this you may start your services
##### mongo:
`sudo docker run --name camp-mongo -d -p 27017 -v ~/data:/data/db mongo`, where ~/data is a directory to mount
##### categories:
`docker run --link=camp-mongo:camp-mongo -p 5857:5857 -e MONGO_HOST='camp-mongo' camp-categories-api
`
##### auth:
`docker run --link=camp-mongo:camp-mongo -p 5858:5858 -e MONGO_HOST='camp-mongo' camp-auth-api`
##### news:
`docker run --link=camp-mongo:camp-mongo -p 5859:5859 -e MONGO_HOST='camp-mongo' camp-news_api `

Also you should to run a proxy server,which will direct requests to microservices

##### proxy:
`gunicorn app:app --reload --bind localhost:{YOUR_PORT} --worker-class aiohttp.GunicornWebWorker`
