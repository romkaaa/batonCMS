from marshmallow import fields, Schema, validates, ValidationError


class AuthSchema(Schema):
    login = fields.String(
        required=True,
        error_messages={'required': "Login is required"}
    )
    password = fields.String(
        required=True,
        error_messages={'required': "Password is required"}
    )


class TokenSchema(Schema):
    token = fields.String(
        required=True,
        error_messages={'required': "Token is required"}
    )


class GetSchema(Schema):
    id = fields.String(
        required=True,
        error_messages={'required': "ID is required"}
    )


class EditSchema(Schema):
    id = fields.String(
        required=True,
        error_messages={'required': "ID is required"}
    )
    login = fields.String(
        required=True,
        error_messages={'required': 'Login is required'}

    )
    password = fields.String(
        required=True,
        error_messages={'required': 'Password is required'}
    )
    email = fields.String(
        required=True,
        error_messages={'required': 'Email is required'}

    )
    name = fields.String(
        required=True,
        error_messages={'required': 'Name is required'}

    )
    token = fields.String(
        required=True,
        error_messages={'required': 'Token is required'}
    )

    @validates('login')
    def validate_mode(self, value):
        if len(value) < 4:
            raise ValidationError('Login must be greater than 4')

    @validates('email')
    def validate_mode(self, value):
        if len(value) < 4:
            raise ValidationError('Email must be greater than 4')

    @validates('name')
    def validate_mode(self, value):
        if len(value) < 4:
            raise ValidationError('Name must be greater than 4')

    # @validates('password')
    # def validate_mode(self, value):
    #     if len(value) < 8:
    #         raise ValidationError('Password must be greater than 8')
