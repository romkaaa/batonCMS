import json
from marshmallow import ValidationError
from aiohttp import web, request
from route_table import routes
from views.models import Users
from views import validator


@routes.view('/users')
class UserView(web.View):

    async def get(self):
        data = await self.request.text()
        try:
            data = json.loads(data)
        except:
            return web.json_response({
                'status': "false",
                'errors': {
                  'jsonParseError': 'params is required'
                                      }})
        try:
            schema = validator.GetSchema()
            result = schema.load(data)
            print(result)
            if len(result.errors) is 0:
                users = Users(data)
                result = await users.get()
                return web.json_response(result)
        except ValidationError as err:
            print(err)
            return web.json_response({'status': "false", 'errors': err.messages})

    async def post(self):
        data = await self.request.text()
        try:
            data = json.loads(data)
        except:
            return web.json_response({
                'status': "false",
                'errors': {
                  'jsonParseError': 'params is required'
                                      }})
        try:
            mode = data['mode']
        except:
            return web.json_response({
                'status': "false",
                'errors': {
                    'jsonParseError': 'mode is required'
                }
            })
        if mode == "tokenCheck":
                try:
                    schema = validator.TokenSchema()
                    result = schema.load(data)
                    if len(result.errors) is 0:
                        users = Users(data)
                        result = await users.check()
                        if result is not False:
                            response = {
                                "status": 200
                            }
                        else:
                            response = {
                                "status": 401
                            }
                        return web.json_response(response)
                    else:
                        return web.json_response({'status': "false", 'errors': result.errors})
                except ValidationError as err:
                    print(err.messages)
                    return web.json_response({'status': "false", 'errors': err.messages})
        if mode == "auth":
            try:
                schema = validator.AuthSchema()
                result = schema.load(data)
                if len(result.errors) is 0:
                    users = Users(data)
                    response = await users.auth()
                    if response is not None:
                        return web.json_response(response)
                    else:
                        return web.json_response({'status': 401})
                else:
                    return web.json_response({'status': "false", 'errors': result.errors})
            except ValidationError as err:
                print(err.messages)
                return web.json_response({'status': "false", 'errors': err.messages})

    async def put(self):
        data = await self.request.text()
        try:
            schema = validator.EditSchema()
            try:
                data = json.loads(data)
            except:
                return web.json_response({'status': "false",
                                          'errors': {
                                              'jsonParseError': 'params is required'
                                          }})
            result = schema.load(data)
            print(result)
            if len(result.errors) is 0:
                users = Users(data)
                if await users.edit() is True:
                    return web.json_response({'status': "true"})
                else:
                    return web.json_response({'status': "false"})
            else:
                return web.json_response({'status': "false", 'errors': result.errors})
        except ValidationError as err:
            print(err.messages)
            return web.json_response({'status': "false", 'errors': err.messages})
