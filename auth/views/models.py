import asyncio
import os
import binascii
import hashlib
from app import app
from datetime import datetime


class Users:
    def __init__(self, data):
        self.data = data

    async def get(self):
        id = self.data['id']
        print("id", id)
        cursor = app.db.users.find({'id': int(id)}, {
            '_id': False,
            'login': True,
            'email': True,
            'name': True
        })
        res_len = await app.db['users'].count_documents({})
        response = await cursor.to_list(res_len)
        return response

    async def check(self):
        token = self.data['token']
        result = await app.db['users'].find_one({'auth_key': token}, {'_id': False, 'token': True})
        if result is not None:
            return True
        else:
            return False

    async def auth(self):
        login = self.data['login']
        password = self.data['password']
        password_to_hash = hashlib.sha1(password.encode())
        password_hash = password_to_hash.hexdigest()
        try:
            results = await app.db['users'].find_one({
                'login': login,
                'password': password_hash
            })
            if results is not None:
                token = binascii.hexlify(os.urandom(30)).decode()
                await app.db['users'].update_one(
                    {
                        'login': login,
                        'password': password_hash
                    },
                    {
                        "$set":
                            {
                                'token': token
                            }
                    }
                )
                auth_data = {
                    'token': token
                }
                return auth_data
            else:
                return False
        except:
            print("ошибка авторизации")

    async def edit(self):
        try:
            password = self.data['password']
            check_status = await self.check()
            print("результат проверки: ", check_status)
            if check_status is True:
                password_to_hash = hashlib.sha1(password.encode())
                password_hash = password_to_hash.hexdigest()
                result = await app.db['users'].update_one(
                    {"id": int(self.data['id'])},
                    {
                        "$set":
                        {
                            'login': self.data['login'],
                            'password': password_hash,
                            'email': self.data['email'],
                            'name': self.data['name']
                        }
                    }
                )
            else:
                return False
            return True
        except:
            return False

    async def remove(self):
        try:
            result = await app.db['users'].delete_many({'id': int(self.data['id'])})
            return True
        except:
            return False
