from aiohttp import web
from route_table import add_routes
from motor.motor_asyncio import AsyncIOMotorClient
import os

app = web.Application()

print("Mongo hostname is ", os.environ['MONGO_HOST'])
mongo_host = os.environ['MONGO_HOST']
uri = 'mongodb://'+mongo_host+':27017'
client = AsyncIOMotorClient(uri)

app.db = client['camp']
app.collection = app.db['users']
app.router.add_routes(add_routes())

