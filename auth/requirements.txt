termcolor==1.1.0
falcon_multipart==0.2.0
requests==2.19.1
falcon==1.4.1
Pillow==5.2.0
aiohttp==3.3.2
motor==2.0.0
pymongo==3.7.1
marshmallow==2.15.3
gunicorn==19.7.1